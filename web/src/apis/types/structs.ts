export interface Notice {
  content: string;
  announcer: string;
  time: string;
  id: number;
}
export interface VolunteerRecord {
  volId: number;
  name: string;
  inside: number;
  outside: number;
  large: number;
  status: number;
}
